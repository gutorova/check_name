const {task, series, parallel, src, dest, watch} = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync');
const notify = require('gulp-notify');
const postcss = require('gulp-postcss');
const csscomb = require('gulp-csscomb');
const autoprefixer = require('autoprefixer');
const mqpacker = require('css-mqpacker');
const sortCSSmq = require('sort-css-media-queries');
const rigger = require('gulp-rigger');
const concat = require('gulp-concat');

const path = {
  scssFolder: './assets/scss/',
  scssFiles: './assets/scss/**/*.scss',
  scssFile: './assets/scss/style.scss',
  cssFolder: './assets/css/',
  cssFile: './assets/css/style.css',
  htmlFiles: './*.html',
  jsFiles: './assets/js/**/*.js',
  jsFolder: './assets/js/',
  libsFolder: './libs/',
  htmlTemplateAll: './assets/html/*.html',
  htmlTemplate: './assets/html/[^_]*.html',
};

const plugins = [
  autoprefixer({
    overrideBrowserslist: [
      'last 5 versions',
      '> 1%'
    ],
    cascade: true
  }),
  mqpacker({sort: sortCSSmq})
];


task('normalize', function() {
  return src('./node_modules/normalize-scss/**/*').pipe(dest(path.libsFolder + 'normalize'));
});
task('jquery', function() {
  return src('./node_modules/jquery/dist/**/*').pipe(dest(path.libsFolder + 'jquery'));
});
task('bootstrap', function() {
  return src('./node_modules/bootstrap/dist/**/*').pipe(dest(path.libsFolder + 'bootstrap'));
});
task('countup', function() {
  return src('./node_modules/countup.js/dist/**/*').pipe(dest(path.libsFolder + 'countup'));
});
task('aos', function() {
  return src('./node_modules/aos/dist/**/*').pipe(dest(path.libsFolder + 'aos'));
});
task('smooth-scroll', function() {
  return src('./node_modules/smooth-scroll/dist/**/*').pipe(dest(path.libsFolder + 'smooth-scroll'));
});
task('jquery.marquee', function() {
  return src('./node_modules/jquery.marquee/*.js').pipe(dest(path.libsFolder + 'jquery.marquee'));
});
task('fancybox', function() {
  return src('./node_modules/@fancyapps/fancybox/dist/**/*').pipe(dest(path.libsFolder + 'fancybox'));
});
task('flickity', function() {
  return src('./node_modules/flickity/dist/**/*').pipe(dest(path.libsFolder + 'flickity'));
});
task('js-concat', function() {
  return src([
      path.libsFolder + 'jquery/jquery.min.js',
      path.libsFolder + 'bootstrap/js/bootstrap.bundle.min.js',
      path.libsFolder + 'aos/aos.js',
      path.libsFolder + 'smooth-scroll/smooth-scroll.min.js',
      path.libsFolder + 'jquery.marquee/jquery.marquee.min.js',
      path.libsFolder + 'fancybox/jquery.fancybox.min.js',
      path.libsFolder + 'flickity/flickity.pkgd.min.js',
      path.libsFolder + 'countup/countUp.min.js',
  ])
    .pipe(concat('app.js'))
    .pipe(dest(path.jsFolder));
});
task('css-concat', function() {
  return src([
    path.libsFolder + 'aos/aos.css',
    path.libsFolder + 'fancybox/jquery.fancybox.min.css',
    path.libsFolder + 'flickity/flickity.min.css',
  ])
      .pipe(concat('app.css'))
      .pipe(dest(path.cssFolder));
});


function html() {
  return src(path.htmlTemplate).
    pipe(rigger()).
    pipe(dest('./')).
    pipe(notify({
      message: 'Compiled html!',
      sound: false
    })).
    pipe(browserSync.reload({stream: true}));
}

function scss() {
  return src(path.scssFile).
    pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError)).
    pipe(postcss(plugins)).
    pipe(dest(path.cssFolder)).
    pipe(notify({
      message: 'Compiled!',
      sound: false
    })).
    pipe(browserSync.reload({stream: true}));
}

function scssDev() {
  return src(path.scssFile, {sourcemaps: true}).
    pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError)).
    pipe(postcss(plugins)).
    pipe(dest(path.cssFolder, {sourcemaps: true})).
    pipe(notify({
      message: 'Compiled!',
      sound: false
    })).
    pipe(browserSync.reload({stream: true}));
}

function comb() {
  return src(path.scssFiles).
    pipe(csscomb()).
    on('error', notify.onError((error) => `File: ${error.message}`)).
    pipe(dest(path.scssFolder));
}

function syncInit() {
  browserSync({
    server: {baseDir: './'},
    notify: false
  });
}

async function sync() {
  browserSync.reload();
}

function watchFiles() {
  syncInit();
  watch(path.htmlTemplateAll, series(html));
  watch(path.scssFiles, series(scss));
  watch(path.htmlFiles, sync);
  watch(path.jsFiles, sync);
}

task('comb', series(comb));
task('scss', series(scss));
task('dev', series(scssDev));
task('html', series(html));
task('lib', series('normalize'));
task('watch', watchFiles);
